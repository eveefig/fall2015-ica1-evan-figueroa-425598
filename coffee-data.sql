CREATE database coffee;
create table coffees( id int unsigned NOT NULL AUTO_INCREMENT, name VARCHAR(50) NOT NULL, primary key (id)) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
 create table ratings( coffeeId int unsigned NOT NULL AUTO_INCREMENT, user VARCHAR(50) NOT NULL, rating DECIMAL(1,1) NOT NULL, comment tinytext, primary key (coffeeId), FOREIGN key (coffeeId) REFERENCES coffees (id)) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;
